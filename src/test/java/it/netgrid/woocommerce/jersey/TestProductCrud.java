package it.netgrid.woocommerce.jersey;

import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;


import it.netgrid.woocommerce.Configuration;
import it.netgrid.woocommerce.CrudService;
import it.netgrid.woocommerce.model.Product;

public class TestProductCrud {

	//IT WORKS
	public void testProductCrud(){
		setSSLContext();
		Configuration c=new Configuration(){

			@Override
			public String getTargetUrl() {
				return "";
			}

			@Override
			public String getBasePath() {
				return "";
			}

			@Override
			public String getConsumerKey() {
				return "";
			}

			@Override
			public String getConsumerSecret() {
				return "";
			}
			
		};
	
		CrudService<Product, Integer, Object> crud=CrudServiceManager.createProductService(c);
		Integer id=18617;
		
		Product p=crud.read(id, null);
		System.out.println(p.getName());
		String newName=p.getName()+" XD";
		p.setName(newName);
		crud.update(p, null);
		Product p2=crud.read(id, null);
		assertTrue(p2.getName().equals(newName));
		
		
	}
	
	public void setSSLContext() {
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput = new FileInputStream("/Users/alberto/Repository/italpet/dst_root_ca.der");
            Certificate ca;
            try {
                ca = cf.generateCertificate(caInput);
                System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
            } finally {
                caInput.close();
            }
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);
            SSLContext.setDefault(context);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
	
}
