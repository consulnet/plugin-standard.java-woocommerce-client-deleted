# Java Woocommerce REST Client (JWCRC) [![Build Status](http://ci.netgrid.it/buildStatus/icon?job=jwcrc)](http://ci.netgrid.it/job/jwcrc) #

JWCRC provides a client for the Woocommerce API v3.

### Requirements ###

JWCRC uses Jersey 2.x as REST Client and Genson as JSON parser / serializer.

### How do I get set up? ###

This library is hosted by the [Central Maven Repository](https://search.maven.org/#search%7Cga%7C1%7Ca%3A%22jwcrc%22), so you can set up your Maven project simply adding the dependency

    <dependencies>
        <dependency>
            <groupId>it.netgrid</groupId>
            <artifactId>jwcrc</artifactId>
            <version>0.0.2</version>
        </dependency>
    </dependencies>

If you want do it from source, download the code and compile it using maven install.

### How it works ###


JWCRC provides two managers, one produces CrudService instances (for simple data manipulation), the other produces BulkService instances (for complex queries and bulk updates).

JWRC doesn't use dependency injection, but supports it providing a strong type definition through interfaces with parametric types.

First of all you have to provide a Configuration implementation. The simplest way is the following:

    Configuration config = new Configuration() {
		
        @Override
        public String getTargetUrl() {
            return "https://www.yourwebsite.com";
        }
		
        @Override
        public String getConsumerSecret() {
            return "{your_consumer_secret}";
        }
		
        @Override
        public String getConsumerKey() {
            return "{your_consumer_key}";
        }
		
        @Override
        public String getBasePath() {
            return "path/to/woocommerce/root";
        }
    };

For more information about obtaining consumer secret and key for your client, visit [the official webpage](https://docs.woothemes.com/document/woocommerce-rest-api/#section-3).

Now, you can obtain a CrudService or a BulkService implementation:

    CrudService<Order, Integer, Object> crudOrderService = CrudServiceManager.createOrderService(config);
    BulkService<Order, Integer, Object> bulkOrderService = BulkServiceManager.createOrderBulkService(config);

CrudService can handle one item per request

    Order order = crudOrderService.read(552, null);
    // ...
    // change order properties
    // ...
    order = crudOrderService.update(order, null);

BulkService can read or create/update a list of items per request

    List<Order> orders = bulkOrderService.read(null);
    for(Order order : orders) {
        // ...
        // change order properties
        // ...
    }
    orders = bulkOrderService.write(orders, null);

The "null" argument in service function calls sets the context for the request. In some cases (eg. Order Notes) you have to provide a valid context for retrieve or write informations.

    CrudService<OrderNote, Integer, Order> crudOrderNoteService = CrudServiceManager.createOrderNoteService(config);
    
    OrderNote orderNote = new OrderNote();
    orderNote.setCreatedAt(new Date());
    orderNote.setCustomerNote(false);
    orderNote.setNote("My personal order note");
    
    crudOrderNoteService.create(orderNote, order);

### TODO ###
We have not yet implemented:

* bulk read paging support
* reports
* deep tax handling support

### Contribution guidelines ###

* Use Git Flow branching strategy
* Preserve the existing codebase coding style
* Write tests if needed
* We review *ALL* your code before merging

### Whom should I speak to? ###

* Repo owner or admin